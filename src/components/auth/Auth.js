import React, { Component } from 'react'
import ApiCalendar from 'react-google-calendar-api'
  
class Auth extends Component {
    constructor(props) {
        super(props);
        this.handleItemClick = this.handleItemClick.bind(this);
    }
    
    handleItemClick(event, name) {
        if (name === 'sign-in') {
            ApiCalendar.handleAuthClick()
        } else if (name === 'sign-out') {
            ApiCalendar.handleSignoutClick()
        }
    }

    render() {
        return (
            <div>
                <button
                    onClick={(e) => this.handleItemClick(e, 'sign-in')}
                >
                    sign-in
                </button>
                <button
                    onClick={(e) => this.handleItemClick(e, 'sign-out')}
                >
                    sign-out
                </button>
            </div>
        )
    }
}

export default Auth