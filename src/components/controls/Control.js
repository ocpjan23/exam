import React, { Component } from 'react'
import ApiCalendar from 'react-google-calendar-api'

class Control extends Component {
    constructor(props) {
        super(props);

    }

    createCal() {
        const eventFromNow = {
            summary: "Poc Dev From Now",
            time: 480,
        };

        ApiCalendar.createEventFromNow(eventFromNow).then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log(error);
        });
    }

    getUpcomingEvents() {
        ApiCalendar.listUpcomingEvents(10)
            .then((result) => {
            console.log(result.result.items);
        });
    }

    render() {
        return (
            <div>
                <button onClick={this.createCal}>Add</button>
                <button onClick={this.getUpcomingEvents}>Get Events</button>
            </div>
        )
    }
}

export default Control