import React, { Component } from 'react';


class Calendar extends Component {

    constructor(props) {
        super(props);
    }

    tableRowItem(_year, _month) {
        let tempTableRow = []
        let controlDate = new Date(_year, _month + 1, 0)
        let currDate = new Date(_year, _month, 1)
        let iter = 0
        let ready = true;
        let trObj = null;

        if (currDate.getDay() !== 0) {
            iter = 0 - currDate.getDay();
        }

        while (ready) {
            if (currDate.getDay() === 6) {
                if (trObj) {
                    tempTableRow.push(
                        <tr key={iter}>
                            {this.tableItems(trObj.tdObj)}
                        </tr>
                    )
                }
                trObj = null
            }

            if (!trObj) {
                trObj = {
                    tdObj: []
                }
            }

            currDate = new Date(_year, _month, ++iter)
            trObj.tdObj.push(this.newDayCell(currDate, iter < 1 || +currDate > +controlDate));

            if (+controlDate < +currDate && currDate.getDay() === 0) {
                ready = false
            }

        }

        return tempTableRow
    }

    newDayCell(dateObj, isOffset) {
        let tdObj = {
            day: dateObj.getDate()
        }
        //tdObj.class = isOffset ? 'day adj-month' : 'day'
        return dateObj.getDate()
    }

    tableItems(_items) {
        let tempTableItems = [];
        for (const [index, value] of _items.entries()) {
            tempTableItems.push(
                <td key={index}>
                    {value}
                </td>
            )
        }
        return tempTableItems
    }
    
    render() {
        const weekHeader = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        let tableWeekHeader = this.tableItems(weekHeader);

        let tableWeekItem = this.tableRowItem(2019, 8);
        
        
        return (
            <div>
                <table>
                    <thead>
                        <tr>
                            {tableWeekHeader}
                        </tr>
                    </thead>
                    <tbody>
                        {tableWeekItem}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Calendar