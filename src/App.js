import React from 'react';
import './App.css';

import Calendar from './components/calendar/Calendar'
import Auth from './components/auth/Auth'
import Control from './components/controls/Control'

function App() {
  return (
    <div className="App">
      <Auth />
      <Calendar />
      <Control />
    </div>
  );
}

export default App;
